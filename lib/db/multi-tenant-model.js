const mongoose = require("mongoose");
const { getCurrentTenantId } = require("../utils/namespace");

const { Schema } = mongoose;

const setSchemaOpts = schema => {
  schema.set("timestamps", true);
  return schema;
};

const tenantModel = (name, schema, options) => props => {
  schema.add({
    workspace: {
      index: true,
      ref: "Workspace",
      required: true,
      type: Schema.Types.ObjectId,
    },
  });

  schema.pre("validate", function setCompanyId() {
    if (!this.get("workspace")) {
      this.set(
        "workspace",
        this.get("__t")
          .split("-")
          .pop(),
      );
    }
  });

  const Model = mongoose.model(name, setSchemaOpts(schema), options);

  if (props && props.skipTenant) {
    return Model;
  }

  // Model.schema.set("discriminatorKey", "workspace");

  const tenantId = (props && props.tenant) || getCurrentTenantId();
  const discriminatorName = `${Model.modelName}-${tenantId}`;
  const existingDiscriminator = (Model.discriminators || {})[discriminatorName];
  return (
    existingDiscriminator ||
    Model.discriminator(discriminatorName, new Schema({}))
  );
};

const tenantlessModel = (name, schema, options) => () =>
  mongoose.model(name, setSchemaOpts(schema), options);

module.exports = {
  tenantModel,
  tenantlessModel,
};
