const { Schema } = require("mongoose");

const User = new Schema({
  active: {
    default: true,
    index: true,
    required: true,
    type: Boolean,
  },
  avatar: String,
  email: {
    index: true,
    required: true,
    type: String,
  },
  name: String,
});

module.exports = User;
