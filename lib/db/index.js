require("dotenv").config();
const mongoose = require("mongoose");

const { tenantModel, tenantlessModel } = require("./multi-tenant-model");

const MemberSchema = require("./schemas/Member");
const UserSchema = require("./schemas/User");
const WorkspaceSchema = require("./schemas/Workspace");

if (process.env.NODE_ENV !== "test") {
  mongoose.connect(process.env.MONGO_DB, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
}

const models = {
  Member: tenantModel("Member", MemberSchema),
  User: tenantlessModel("User", UserSchema),
  Workspace: tenantlessModel("Workspace", WorkspaceSchema),
};

// make sure all of these have been initialized
Object.values(models).forEach(v => v({ skipTenant: true }));

module.exports = models;
