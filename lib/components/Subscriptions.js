import React from "react";
import PropTypes from "prop-types";
import Router from "next/router";

import { Box, Button, Grid, Heading, Paragraph } from "grommet";
import { Checkmark } from "grommet-icons";

import { checkFeature, features } from "../utils/features";
import getConfig from "../config";
import http from "../utils/http";

const Subscriptions = ({ billing, products = [] }) => {
  const subscribe = async plan => {
    if (!billing) {
      window.sessionStorage.setItem("redirectTo", "/billing");
      Router.push("/login");
      return;
    }

    if (billing && billing.planId === plan) {
      return;
    }

    const { sessionId } = await http(
      `/api/billing/session?plan=${plan}&redirectPage=${
        getConfig().APP_URL
      }/billing`,
    );

    const stripe = window.Stripe(getConfig().STRIPE_PUBLISHABLE_KEY);
    stripe.redirectToCheckout({ sessionId });
  };

  return (
    <Box>
      {billing && billing.planId && !billing.expired ? (
        <Box background="light-1" elevation="small" pad="medium" round="small">
          <Heading level={5} margin="none" style={{ fontWeight: 400 }}>
            Current Subscription
          </Heading>
          <Heading level={4} margin="none">
            {billing.planLabel}
          </Heading>
          <Paragraph>
            Automatically renews {new Date(billing.renews).toLocaleDateString()}
          </Paragraph>

          {billing.planId ? (
            <Box direction="row">
              <Button
                color="neutral-3"
                label="Update Payment Method"
                margin={{ right: "small" }}
                onClick={() => subscribe("update")}
              />

              <form
                action={`/api/billing/cancel?redirectTo=${
                  getConfig().DEFAULT_ROUTE
                }`}
                method="POST"
              >
                <Button
                  color="status-critical"
                  label="Cancel Subscription"
                  type="submit"
                />
              </form>
            </Box>
          ) : null}
        </Box>
      ) : null}

      <Grid columns="medium" gap="medium" margin={{ top: "large" }}>
        {(products || [])
          .sort((a, b) => {
            const v1 = Math.min(...(a.plans || []).map(p => p.amount));
            const v2 = Math.min(...(b.plans || []).map(p => p.amount));

            return v1 > v2 ? 1 : -1;
          }, [])
          .map(product => (
            <Box
              align="center"
              background="light-1"
              elevation="small"
              key={product.id}
              pad="medium"
              round="small"
            >
              <Heading level={3}>{product.name}</Heading>
              {Object.keys(features)
                .filter(k => checkFeature(product.name, k))
                .map(k => (
                  <Paragraph key={k}>
                    <Checkmark size="small" style={{ marginRight: "0.5em" }} />
                    {features[k].label}
                  </Paragraph>
                ))}
              <Box direction="row" fill="horizontal">
                {(product.plans || [])
                  .sort((a, b) => (a.amount > b.amount ? 1 : -1), [])
                  .map(plan => (
                    <Button
                      key={plan.id}
                      label={
                        billing && plan.id === billing.planId
                          ? "Active"
                          : `$${(plan.amount / 100).toFixed(2)} / ${
                              plan.interval
                            }`
                      }
                      margin="small"
                      onClick={() => subscribe(plan.id)}
                      primary={billing && plan.id === billing.planId}
                      style={{ width: 200 }}
                    />
                  ))}
              </Box>
            </Box>
          ))}
      </Grid>
    </Box>
  );
};

Subscriptions.propTypes = {
  billing: PropTypes.object,
  products: PropTypes.any,
};

export default Subscriptions;
