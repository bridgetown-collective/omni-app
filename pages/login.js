import React from "react";
import PropTypes from "prop-types";

import withLayout from "../lib/components/hocs/with-layout";

const LoginPage = ({ auth }) => {
  React.useEffect(() => {
    if (auth && auth.service) {
      auth.service.login();
    }
  }, [auth]);

  return "Logging in...";
};

LoginPage.propTypes = {
  auth: PropTypes.any,
};

export default withLayout(LoginPage, { authRequired: false, title: "Login" });
