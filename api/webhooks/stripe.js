require("dotenv").config();
const bodyParser = require("body-parser");
const express = require("express");
const moment = require("moment");
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const db = require("../../lib/db");
const { plansByName } = require("../../lib/utils/features");

const router = express();

router.post(
  "*",
  bodyParser.raw({ type: "application/json" }),
  async (req, res) => {
    const sig = req.headers["stripe-signature"];

    let event;

    try {
      event = stripe.webhooks.constructEvent(
        req.body,
        sig,
        process.env.STRIPE_SIGNING_SECRET,
      );
    } catch (err) {
      res.status(400).send(`Webhook Error: ${err.message}`);
      return;
    }

    // Handle the checkout.session.completed event
    if (event.type === "checkout.session.completed") {
      const session = event.data.object;

      try {
        const workspace = await db
          .Workspace()
          .findOne({ "billing.session": session.id });
        if (workspace) {
          try {
            if (
              session.mode !== "setup" &&
              session.subscription !== workspace.get("billing.subscription")
            ) {
              await stripe.subscriptions.del(
                workspace.get("billing.subscription"),
              );
            }
          } catch (e) {
            // ignore this
          }

          const plan = await stripe.plans.retrieve(
            session.display_items[0].plan.id,
          );
          const product = await stripe.products.retrieve(plan.product);

          await workspace
            .set({
              billing: {
                created: Date.now(),
                customer: session.customer,
                expired: false,
                planId: plan.id,
                planLabel: `${product.name} (${plan.nickname})`,
                renews: moment()
                  .add(plan.interval_count, plan.interval)
                  .toDate()
                  .getTime(),
                session: undefined,
                subscription: session.subscription,
              },
              plan: plansByName[product.name],
            })
            .save();
        }
      } catch (e) {
        // ignore this
      }
    }

    if (event.type === "customer.subscription.deleted") {
      try {
        const workspace = await db
          .Workspace()
          .findOne({ "billing.subscription": event.data.object.id });
        if (workspace) {
          await workspace
            .set({
              billing: {
                created: Date.now(),
                expired: false,
                planId: null,
              },
              plan: null,
            })
            .save();
        }
      } catch (e) {
        // ignore this
      }
    }

    res.json({ received: true });
  },
);

module.exports = router;
