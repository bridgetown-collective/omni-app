const router = require("../lib/utils/router")();
const uploader = require("../lib/utils/upload-middleware")({
  directory: "profile-avatars",
});
const responseMiddleware = require("../lib/utils/response-middleware");

router.post("*", uploader.single("avatar"), async (req, res, next) => {
  if (req.file && req.file.location) {
    req.body.avatar = req.file.location;
  }

  try {
    await req.user
      .set({
        avatar: req.body.avatar || req.user.get("avatar"),
        name: req.body.name,
      })
      .save();
    req.message = "Success";
  } catch (err) {
    req.error = err;
  }

  next();
});

router.use(responseMiddleware);

module.exports = router;
